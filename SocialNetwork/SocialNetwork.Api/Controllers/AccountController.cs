﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SocialNetwork.Api.App_Start;
using SocialNetwork.Api.Models;
using SocialNetwork.Api.Models.Account;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace SocialNetwork.Api.Controllers
{
    
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };
            
            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        // POST: /Account/Login
        //[HttpPost]
        //[AllowAnonymous]
        //public async Task<IHttpActionResult> Login(FormUrlEncodedContent requestContent)
        //{
        //    requestContent.
        //    var tokenData = JObject.Parse(responseContent);
        //    _tokenHelper.AccessToken = tokenData["access_token"];

        //    var result = await SignInManager.PasswordSignInAsync(model["username"], model["password"], false, shouldLockout: true);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            var user = await UserManager.FindAsync(model["username"], model["password"]);

        //            await SignInAsync(user);
        //            return Ok();
        //        case SignInStatus.LockedOut:
        //            //return View("Lockout");
        //        case SignInStatus.RequiresVerification:
        //            //return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
        //        case SignInStatus.Failure:
        //        default:
        //            return BadRequest();
        //            //ModelState.AddModelError("", "Login ou Senha incorretos.");
        //            //return View(model);
        //    }
        //}
        //private async Task SignInAsync(ApplicationUser user)
        //{
        //    var clientKey = Request.Browser.Type;
        //    await UserManager.SignInClientAsync(user, clientKey);
        //    // Zerando contador de logins errados.
        //    await UserManager.ResetAccessFailedCountAsync(user.Id);

        //    // Coletando Claims externos (se houver)
        //    ClaimsIdentity ext = await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);

        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie, DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn
        //        (
        //            new AuthenticationProperties { IsPersistent = isPersistent },
        //            // Criação da instancia do Identity e atribuição dos Claims
        //            await user.GenerateUserIdentityAsync(UserManager, ext)
        //        );
        //}







        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        
    }
}
