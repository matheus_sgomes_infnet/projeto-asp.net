﻿using Microsoft.Owin.Security.OAuth;
using System;
using Microsoft.Owin;
using Owin;
using SocialNetwork.Api.Models;
using SocialNetwork.Api.Providers;
using SocialNetwork.Api.App_Start;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

namespace SocialNetwork.Api
{
    //public partial class Startup
    //{
    //    public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

    //    public void ConfigureAuth(IAppBuilder app)
    //    {
    //        app.CreatePerOwinContext(ApplicationDbContext.Create);
    //        app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

    //        OAuthOptions = new OAuthAuthorizationServerOptions
    //        {
    //            Provider = new ApplicationOAuthProvider(),
    //            TokenEndpointPath = new PathString("/Token"),
    //            AccessTokenExpireTimeSpan = TimeSpan.FromDays(1)

    //        };

    //        app.UseOAuthBearerTokens(OAuthOptions);
    //    }


    //}

    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);


            //app.UseCookieAuthentication(new CookieAuthenticationOptions());
            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                //AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                // In production mode set AllowInsecureHttp = false
                //AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}