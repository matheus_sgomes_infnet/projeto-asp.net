﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SocialNetwork.Web.Controllers
{

    [Authorize]
    public class SocialNetworkController : Controller
    {
        // GET: SocialNetwork
        public ActionResult Index()
        {
            return View();
        }
    }
}